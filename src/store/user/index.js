import { getUserInfo } from '@/api'
const state = {
  info: {}
}

const mutations = {
  SET_USER_INFO (state, userInfo) {
    state.info = userInfo
  }
}

const actions = {
  async GetUserInfo ({ commit }) {
    const userInfo = (await getUserInfo()).data.data.currentUser
    commit('SET_USER_INFO', userInfo)
    return Promise.resolve(userInfo)
  }
}

export default {
  state,
  mutations,
  actions
}
