import request from './request'

export const getUserInfo = () => {
  return request({
    url: '/v/currentuser'
  })
}
